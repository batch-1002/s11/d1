
// alert('hello world');

let productName = "Desktop computer";
let productPrice = 18999;
const PI = 3.1416;

console.log(productName);
console.log(productPrice);
console.log(PI);


let statement1 = "true";
let statement2 = true;
let statement3 = statement1 == statement2;
let statement4 = statement1 === statement2;
console.log(statement3);
console.log(statement4);

function createFullName(fName, mName, lName) {
	return fName + " " + mName + ", " + lName;
};

function addThenDouble(num1, num2) {
	return 2 * (num1 + num2);
};

console.log(addThenDouble(5, 5));